from django.test import TestCase
from django.views.decorators.csrf import csrf_exempt
from django.test import Client
from django.contrib.auth.models import User


class GetTests(TestCase):
    # Test sobre GET de la pagina principal
    @csrf_exempt
    def test_root(self):
        c = Client()
        response = c.get('/lovisto/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<br><h3><b>Ultimas 10 aportaciones del sitio </b></h3>', content)

    @csrf_exempt
    def test_todas_aportaciones(self):
        # Test de /aportaciones
        c = Client()
        response = c.get('/lovisto/aportaciones')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3> Página de todas las aportaciones </h3>', content)

    @csrf_exempt
    def test_info(self):
        # Test de /info
        c = Client()
        response = c.get('/lovisto/info')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3> Página de informacion </h3>', content)


    @csrf_exempt
    def test_usuario(self):
        # Test GET de la página de usuario (sin estar logeado)
        c = Client()
        response = c.get('/lovisto/usuario')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("Te acabas de deslogear, por tanto no puedes observar esta pagina. Vuelva a la pagina principal.", content)


    @csrf_exempt
    def test_usuario_logeado(self):
        #Test GET de la pagina de usuario (estando logeado)
        c = Client()
        user = User.objects.create(username='usuario')
        user.set_password('usuario')
        user.save()

        logged_in = c.login(username='usuario', password='usuario')
        self.assertTrue(logged_in)
        response = c.get('/lovisto/usuario')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("<h3> Página de usuario </h3>", content)

class PostTest(TestCase):
    @csrf_exempt
    def test_POST_añadir_aportacion(self):
        # Test para añadir una aportacion en la pagina principal
        c = Client()
        url = "https://www.urjc.es/"
        response = c.post('/lovisto/', {'action': "Enviar Aportacion", 'titulo': "test", 'descripcion': "probando los test",
                               'url': url})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<br><h3><b>Ultimas 10 aportaciones del sitio </b></h3>', content)

    @csrf_exempt
    def test_Comentario(self):
        # Test para añadir comentarios en la pagina de la aportacion
        c = Client()
        #Creamos el usuario
        usuario = User.objects.create(username='usuario')
        usuario.set_password('usuario')
        usuario.save()
        #Nos logeamos
        logged_in = c.login(username='usuario', password='usuario')
        self.assertTrue(logged_in)
        # Creamos la aportacion
        url = "https://www.urjc.es/"
        response = c.post('/lovisto/',
                          {'action': "Enviar Aportacion", 'titulo': "test", 'descripcion': "probando los test",
                           'url': url})
        self.assertEqual(response.status_code, 200)
        #Creamos el comentario
        response = c.post('/lovisto/aportacion/1', {'action': "Enviar comentario", 'comentario': "este es el comentario"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<br><h3><b>Ultimas 10 aportaciones del sitio </b></h3>', content)


    @csrf_exempt
    def test_positivo(self):
        # Test en / para el voton de valor de manera positiva
        c = Client()
        # creamos un usuario y nos logeamos
        usuario = User.objects.create(username='usuario')
        usuario.set_password('usuario')
        usuario.save()

        logged_in = c.login(username='usuario', password='usuario')
        self.assertTrue(logged_in)
        url = "https://www.urjc.es/"
        c.post('/lovisto/',{'action': "Enviar Aportacion", 'titulo': "test", 'descripcion': "probando los test",
                           'url': url})

        response2 = c.post('/lovisto/aportacion/1', {'action': 'positivo'})
        self.assertEqual(response2.status_code, 200)

    @csrf_exempt
    def test_negativo(self):
        # Test en / para el voton de valorar de manera negativa
        c = Client()
        usuario = User.objects.create(username='usuario')
        usuario.set_password('usuario')
        usuario.save()

        logged_in = c.login(username='usuario', password='usuario')
        self.assertTrue(logged_in)
        url = "https://www.urjc.es/"
        c.post('/lovisto/',{'action': "Enviar Aportacion", 'titulo': "test", 'descripcion': "probando los test",
                           'url': url})

        response2 = c.post('/lovisto/aportacion/1', {'action': 'negativo'})
        self.assertEqual(response2.status_code, 200)