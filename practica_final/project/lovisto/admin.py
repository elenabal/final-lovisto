from django.contrib import admin
from .models import Aportaciones, Comentario, Voto

# Register your models here.
admin.site.register(Aportaciones) #para registrar el moelo contenido
admin.site.register(Comentario)
admin.site.register(Voto)