from django.urls import path
from . import views


urlpatterns = [
    path('',views.index),
    path('info',views.info),
    path('usuario',views.usuario),
    path('aportaciones',views.aportaciones),
    path('aportacion/<int:id>', views.aportacion)
]