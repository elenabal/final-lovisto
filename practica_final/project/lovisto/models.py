from django.db import models

# Create your models here.
class Aportaciones(models.Model):
    url = models.TextField()
    titulo = models.CharField(max_length=128)
    descripcion =models.TextField()
    usuario = models.TextField()
    fecha = models.DateTimeField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    info = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.titulo

class Comentario (models.Model):
    comentario = models.TextField()
    usuario = models.TextField()
    fecha = models.DateTimeField()
    aportacion = models.ForeignKey(Aportaciones,on_delete = models.CASCADE) #Podemos tener varios comentarios para una aportacion
    def __str__(self):
        return str(self.id) + ": " + self.titulo

class Voto (models.Model):
    # Solo aceptamos estos votos
    VOTOS = [
        ('P', 'Positivo'),
        ('N', 'Negativo')
    ]
    aportacion = models.ForeignKey(Aportaciones,on_delete = models.CASCADE);
    usuario = models.TextField()
    voto = models.CharField(max_length=1, choices = VOTOS)
    def __str__(self):
        return str(self.aportacion.titulo) + ": " + str(self.usuario)