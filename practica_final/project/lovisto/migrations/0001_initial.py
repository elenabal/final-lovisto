# Generated by Django 3.1.7 on 2021-06-10 08:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aportaciones',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.TextField()),
                ('titulo', models.CharField(max_length=128)),
                ('descripcion', models.TextField()),
                ('usuario', models.TextField()),
                ('fecha', models.DateTimeField()),
                ('votos_positivos', models.IntegerField()),
                ('votos_negativos', models.IntegerField()),
                ('info', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Voto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.TextField()),
                ('voto', models.CharField(choices=[('P', 'Positivo'), ('N', 'Negativo')], max_length=1)),
                ('aportacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.aportaciones')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.TextField()),
                ('usuario', models.TextField()),
                ('fecha', models.DateTimeField()),
                ('aportacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.aportaciones')),
            ],
        ),
    ]
