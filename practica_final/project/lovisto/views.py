from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from .models import Aportaciones,Voto,Comentario
from django.utils import timezone
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.http import JsonResponse
from .forms import ComentarioForm
import xmltodict
import urllib.request
import requests
import json
from bs4 import BeautifulSoup

#Recurso conocido : aemet

def aemet(recurso):
    url_xml = "https://www.aemet.es/xml/municipios/localidad_" + recurso + ".xml"
    handler = urllib.request.urlopen(url_xml) #Para obtener un manejador
    d = xmltodict.parse(handler) #Obtenemos el documento xml y nos devuelve los atributos de la estructura xml
    dias = []
    for dia in d['root']['prediccion']['dia']:
        dias.append("Estadísticas para el: " + dia['@fecha'])
        dias.append("Temperatura : " + dia['temperatura']['minima'] + "/" + dia['temperatura']['maxima'])
        dias.append("Sensacion: " + dia['sens_termica']['minima'] + "/" + dia['sens_termica']['maxima'])
        dias.append("Humedad : " + dia['humedad_relativa']['minima'] + "/" + dia['humedad_relativa']['maxima'])
        dias.append("</br>")
    StringDias = ""
    for dia in dias:
        StringDias = StringDias + "<li>" + dia + "</li>"

    info = "<p><b>Datos AEMET para " + d['root']['nombre'] + "(" + d['root']['provincia'] + ") </p></b>" + "<ul>" + StringDias + "</ul"
    url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/" + d['root']['nombre'] + "-id" + recurso
    info = info + "<p>Copyright AEMET  <a href= '" + url + "'>Artículo original</a></p>"
    return info

# Recurso conocido: youtube
def youtube(recurso):
    url_json = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + recurso
    url = requests.get(url_json)
    text = url.text #Coge el texto de json
    datos = json.loads(text) #Cargamos el json
    info = datos['title'] + "<br>" + datos['html'] + "<br>" + datos['author_name'] #Guardamos la informacion extendida
    return info

#Recurso conocido: reddit
def reddit(recurso, subredit):
    url_json = "https://www.reddit.com/r/" + subredit + "/comments/" + recurso + "/.json"
    page = urllib.request.urlopen(url_json) #Abrimos
    text = page.read().decode('utf-8')
    datos = json.loads(text)
    url = datos[0]['data']['children'][0]['data']['url']
    title = datos[0]['data']['children'][0]['data']['title']
    subreddit = datos[0]['data']['children'][0]['data']['subreddit']
    selftext = datos[0]['data']['children'][0]['data']['selftext']
    upvote_ratio = str(datos[0]['data']['children'][0]['data']['upvote_ratio'])
    if not ("https://i.redd.it/") in url:
        info = "<br>" + title + "<br>" + selftext + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote_ratio + "<br>"
    else:
        info = "<br>" + title + "<br>" + "<img src= '" + url + "' width='250' height='250'>" + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote + "<br>"

    return info


# Funcion para recursos no reconocidos
def recursos_noreconocidos(url):
    info = ""
    page = urllib.request.urlopen(url) #Cargar la pagina de la url
    if not page: #si no existe la pagina
        info = "Información extendida no disponible"
    else:
        soup = BeautifulSoup(page, 'html.parser')
        ogTitle = soup.find('meta', property='og:title')
        if ogTitle: #Si existe la propiedad opengraph
            ogImage = soup.find('meta', property='og:image')
            if ogImage:
                imagen = ogImage['content']
                info = "<br>" + ogTitle['content'] + "<br><img src= '" + imagen + "' width='250' height='150'>"
        elif not ogTitle:
            hTitle = soup.title.string #Para ver si existe algun elemento que se llame titulo si no esta en las propiedad OG
            if hTitle:
                info = hTitle
            else:
                info = "Información extendida no disponible"
    return info

def autenticar(request):
     if request.method == "POST":
         if request.POST['action'] == 'Logout':
             logout(request)
         if request.POST['action'] == 'Login':
             username = request.POST['username']
             password = request.POST['password']
             user = authenticate(request, username=username, password=password)
             if user is not None:
                 login(request, user)

#Funcion para añadir votos positivos a una aportacion
def añadirVotoPositivo(username, aportacion):
    votado = False #Si no he votado anteriormente
    votos = Voto.objects.all().filter(aportacion=aportacion)
    for voto in votos:
        if voto.usuario == username:
            votado = True #Si ya he votado con anterioridad
            if voto.voto == 'P':
                return;
            else: #Si yo quiero votar positivo, pero antes habia votado negativo
                aportacion.votos_positivos = aportacion.votos_positivos + 1
                aportacion.votos_negativos = aportacion.votos_negativos - 1
                voto.voto = 'P'
                aportacion.save()
                voto.save()
    if not votado: #Si nunca habia votado esta aportacion
            aportacion.votos_positivos = aportacion.votos_positivos + 1
            nuevoVoto= Voto(aportacion=aportacion, usuario=username, voto='P')
            nuevoVoto.save()
            aportacion.save()

#Funcion para añadir votos negativos a una aportacion
def añadirVotoNegativo(username, aportacion):
    votado = False

    votos = Voto.objects.all().filter(aportacion=aportacion)
    for voto in votos:
        if voto.usuario == username:
            votado = True
            if voto.voto == 'N':
                return;
            else:
                aportacion.votos_positivos = aportacion.votos_positivos - 1
                aportacion.votos_negativos = aportacion.votos_negativos + 1
                voto.voto = 'N'
                aportacion.save()
                voto.save()
    if not votado:
        aportacion.votos_negativos = aportacion.votos_negativos + 1
        nuevoVoto= Voto(aportacion=aportacion, usuario=username, voto='N')
        nuevoVoto.save()
        aportacion.save()

# Pagina principla : Vista cuando nos piden /
@csrf_exempt
def index(request):
    ## Por defecto los modos
    modo_claro = False
    modo_oscuro = True

    autenticar(request)
    if request.method == "POST":
        if request.POST['action'] == "Enviar aportacion":
            titulo = request.POST['titulo']
            descripcion = request.POST['descripcion']
            url = request.POST['url']
            if "http://" in url or "https://" in url:
                url = url
            else:
                url = "http://" + url
            if ("youtube.com") in url:
                recurso = url.split("=")[1]
                info = youtube(recurso)
            elif ("aemet.es") in url:
                recurso = url.split("id")[1]
                info = aemet(recurso)
            elif ("reddit.com") in url:
                recurso = url.split("/")[6]
                subredit = url.split("/")[4]
                info = reddit(recurso, subredit)
            else:
                info = recursos_noreconocidos(url)
            try:
                aportacion = Aportaciones.objects.get(url=url)
                aportacion.titulo = titulo
                aportacion.descripcion = descripcion
                aportacion.url = url
                aportacion.usuario = request.user.username
                aportacion.fecha = timezone.now()
                aportacion.votos_positivos = 0;
                aportacion.votos_negativos = 0;
                aportacion.info = info;
            except Aportaciones.DoesNotExist:
                aportacion = Aportaciones(titulo=titulo,descripcion=descripcion,url=url,fecha=timezone.now(),
                                          votos_positivos = 0, votos_negativos = 0, usuario = request.user.username,
                                          info=info)
            aportacion.save()
        elif request.POST['action'] == "positivo":
            aportacion = Aportaciones.objects.get(id=request.POST['id'])
            añadirVotoPositivo(request.user.username, aportacion)
        elif request.POST['action'] == "negativo":
            aportacion = Aportaciones.objects.get(id=request.POST['id'])
            añadirVotoNegativo(request.user.username, aportacion)
        if request.POST['action'] == "oscuro":
            modo_claro = False
            modo_oscuro = True
        if request.POST['action'] == "claro":
            modo_claro = True
            modo_oscuro = False

    aportacion_list = Aportaciones.objects.all()
    last_10_aportaciones = aportacion_list.order_by('-fecha')[:10]
    last_5_aportaciones = aportacion_list.order_by('-fecha')[:5]
    last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
    template = loader.get_template('lovisto/principal.html')
    context = {
        'last_10_aportaciones': last_10_aportaciones,
        'last_5_aportaciones': last_5_aportaciones,
        'usuario': request.user.username,
        'autenticado': request.user.is_authenticated,
        'last_3_aportaciones': last_3_aportaciones,
        'modo_oscuro': modo_oscuro,
        'modo_claro': modo_claro
    }
    return HttpResponse(template.render(context,request))

# Pagina de todas las aportaciones : Vista cuando nos piden /aportaciones
@csrf_exempt
def aportaciones(request):
    ## Por defecto los modos
    modo_claro = False
    modo_oscuro = True
    if request.GET.get("format") == "json/":
        queryset = Aportaciones.objects.filter().values()
        return JsonResponse({"Contenidos": list(queryset)})
    if request.GET.get("format") == "xml/":
        template = loader.get_template('lovisto/aportaciones.xml')
        context = {
                    'aportaciones': Aportaciones.objects.all()
                    }
        return HttpResponse(template.render(context, request))

    autenticar(request)
    if request.method == "POST":
        if request.POST['action'] == "oscuro":
            modo_claro = False
            modo_oscuro = True
        if request.POST['action'] == "claro":
            modo_claro = True
            modo_oscuro = False
    template = loader.get_template('lovisto/aportaciones.html')
    aportacion_list = Aportaciones.objects.all().order_by()
    ultimas_aportaciones = aportacion_list.order_by('-fecha')
    last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
    context = {'ultimas_aportaciones': ultimas_aportaciones,
               'autenticado': request.user.is_authenticated,
               'usuario': request.user.username,
                'last_3_aportaciones': last_3_aportaciones,
               'modo_claro': modo_claro,
               'modo_oscuro': modo_oscuro
               }
    return HttpResponse(template.render(context, request))

# Pagina de cada aportacion: Vista cuando nos piden /<id_aportacion>
@csrf_exempt
def aportacion(request,id):
    ## Por defecto los modos
    modo_claro = False
    modo_oscuro = True

    autenticar(request)
    try:
        aportacion = Aportaciones.objects.get(id=id)
    except Aportaciones.DoesNotExist:
        aportacion_list = Aportaciones.objects.all()
        last_10_aportaciones = aportacion_list.order_by('-fecha')[:10]
        last_5_aportaciones = aportacion_list.order_by('-fecha')[:5]
        last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
        context = {'autenticado': request.user.is_authenticated,
                   'last_10_aportaciones': last_10_aportaciones,
                   'last_5_aportaciones': last_5_aportaciones,
                   'usuario': request.user.username,
                   'last_3_aportaciones': last_3_aportaciones,
                   }
        return render(request, 'lovisto/principal.html', context) #Redireccion a la pagina principal
    if request.method == "POST":
        if request.POST['action'] == 'Enviar comentario':
            form = ComentarioForm(request.POST)
            if form.is_valid():
                q = Comentario(aportacion=aportacion, fecha=timezone.now(),usuario=request.user.username,
                               comentario=request.POST['comentario'])
                q.save()
        elif request.POST['action'] == 'positivo':
            añadirVotoPositivo(request.user.username, aportacion)
        elif request.POST['action'] == 'negativo':
            añadirVotoNegativo(request.user.username, aportacion)
        if request.POST['action'] == "oscuro":
            modo_claro = False
            modo_oscuro = True
        if request.POST['action'] == "claro":
            modo_claro = True
            modo_oscuro = False

    comentarios = Comentario.objects.all().filter(aportacion=aportacion)
    aportacion_list = Aportaciones.objects.all()
    last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
    template = loader.get_template('lovisto/aportacion.html')
    context = {'autenticado': request.user.is_authenticated,
               'usuario': request.user.username,
               'aportacion': aportacion,
               'comentarios': comentarios,
               'last_3_aportaciones': last_3_aportaciones,
               'modo_claro': modo_claro,
               'modo_oscuro': modo_oscuro
               }
    return HttpResponse(template.render(context, request))

#Pagina de informacion: Vista cuando nos piden /info
@csrf_exempt
def info(request):
    ## Por defecto los modos
    modo_claro = False
    modo_oscuro = True

    autenticar(request)
    if request.method == "POST":
        if request.POST['action'] == "oscuro":
            modo_claro = False
            modo_oscuro = True
        if request.POST['action'] == "claro":
            modo_claro = True
            modo_oscuro = False

    template = loader.get_template('lovisto/info.html')
    aportacion_list = Aportaciones.objects.all()
    last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
    context = {'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'last_3_aportaciones': last_3_aportaciones,
               'modo_claro': modo_claro,
               'modo_oscuro': modo_oscuro
                }
    return HttpResponse(template.render(context,request))

#Pagina de usuario: Vista cuando nos piden /usuario
@csrf_exempt
def usuario(request):
    ## Por defecto los modos
    modo_claro = False
    modo_oscuro = True

    autenticar(request)
    if request.method == "POST":
        if request.POST['action'] == "oscuro":
            modo_claro = False
            modo_oscuro = True
        if request.POST['action'] == "claro":
            modo_claro = True
            modo_oscuro = False
    if request.user.is_authenticated:  # si el usuario esta autentificado
        comentarios = Comentario.objects.all().filter(usuario=request.user.username)
        aportacion_list = Aportaciones.objects.all()
        last_3_aportaciones = aportacion_list.order_by('-fecha')[:3]
        template = loader.get_template('lovisto/usuario.html')
        context = {'autenticado': request.user.is_authenticated,
                   'usuario': request.user.username,
                   'aportaciones': Aportaciones.objects.all(),
                    'comentarios': comentarios,
                    'votos': Voto.objects.all().filter(usuario=request.user.username),
                   'last_3_aportaciones': last_3_aportaciones,
                   'modo_claro': modo_claro,
                   'modo_oscuro': modo_oscuro
                   }
        return HttpResponse(template.render(context, request))
    else:
        respuesta = "Te acabas de deslogear, por tanto no puedes observar esta pagina. Vuelva a la pagina principal."
        return HttpResponse(respuesta)