# Final Lo Visto

Práctica final del curso 2020/21

# Entrega practica
## Datos
* Nombre: Elena Ballesteros Atienza
* Titulacion: Ingenieria en tecnologias de la telecomunicacion
* Despliegue (url): http://elenabal.pythonanywhere.com/lovisto/
* Video basico (url): https://www.youtube.com/watch?v=H8ls2mh1ms8


## Cuenta Admin Site
* admin/admin
## Cuentas usuarios
* pepe/pepe
* ana/ana

## Resumen parte obligatoria
Esta aplicacion web consiste en la gestion de aportaciones subidas por los usuarios (cabe destacar que los usuarios deben estar inscritos en la aplicacion para poder subir aportaciones). Dichas aportaciones, son enlaces (URLS) a paginas webs, videos u otra informacion que le parezca importante a los usuarios. Cuando dichas aportaciones correspondan con ciertos patrones reconocidos por nuestra aplicacion, se mostrara cierta informacion ofrecida por estos sitios,es decir, una informacion mas detallada sobre la aportacion.
Los usuarios, si estan inscritos en nuestra aplicacion, podran ademas de subir dichas aportaciones, comentarlas y votarlas.

## Lista partes opcionales
* Nombre parte: Favicon

